import {Request,Response,NextFunction } from "express";
import * as logger from "../utilities/logger.js";
import { HttpError, httpResponseMessage } from "../utilities/types.js";
export const addErrorLog = (error: HttpError, req: Request, res: Response, next: NextFunction) => {
    logger.addErrorLog(error.toString());
    next(error);
};

export const sendErrorMessage = (error: HttpError, req: Request, res: Response, next: NextFunction) => {
   console.log({error});
    const resMessage:httpResponseMessage ={
        status: error.statusCode,
        message: error.message,
        data: "request failed"
    };
    res.status(error.statusCode).send(resMessage);
};


export const not_found =  (req:Request, res:Response,next:NextFunction) => {
    next(new HttpError("Error 404 not found route",404));
};


