export class HttpError extends Error{
    constructor(public message: string,public statusCode:number){
        super(message);
    }

    toString():string {
        return `Status code: ${this.statusCode}, Message: ${this.message}
            Stack: ${this.stack}
            `;
    }
}

export interface httpResponseMessage{
    status: number;
    message: string;
    data: any;
}

export interface IIndexObject{
    [index:string]:any
}

export interface ISong {
    name: string;
    length: string;
    artists: string[];
    playlists: string[];
}

export type SongDetailsForUpdate = Partial<ISong>;

export interface IPlaylist {
    name: string;
    songs: string[];
}

export type PlaylistDetailsForUpdate = Partial<IPlaylist>;

export interface IArtist {
    first_name: string;
    last_name: string;
    songs: string[];
}

export type ArtistDetailsForUpdate = Partial<IArtist>;