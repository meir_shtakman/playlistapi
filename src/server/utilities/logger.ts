import fsp from "fs/promises";
import fs from "fs";

export function addLog(details: string) {
    if (fs.existsSync("../logger.log")) {
        fsp.appendFile("../logger.log", details, "utf8");
    } else {
        fsp.writeFile("../logger.log", details, "utf8");
    }
}

export function addErrorLog(details: string) {
    if (fs.existsSync("../errorLogger.log")) {
        fsp.appendFile("../errorLogger.log", details, "utf8");
    } else {
        fsp.writeFile("../errorLogger.log", details, "utf8");
    }
}