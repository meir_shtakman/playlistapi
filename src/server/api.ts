// require('dotenv').config();
import express from "express";
import morgan from "morgan";
import log from "@ajar/marker";
import cors from "cors";
import * as logger from "./utilities/logger.js";

import { connect_db } from "./db/mongoose.connection.js";
import song_router from "./modules/Song/song.router.js";
import artist_router from "./modules/Artist/artist.router.js";

import playlist_router from "./modules/Playlist/playlist.router.js";


import { addErrorLog, sendErrorMessage, not_found } from "./middleware/errors.handler.js";

const { PORT = 8080, HOST = "localhos", DB_URI } = process.env;

class apiUserServer {

  private app ; 


  constructor() {

    this.app = express();
    this.addMiddlewares();
    this.addRoutes();
    this.addErrorsMiddlewares();

  }
  
  addErrorsMiddlewares() {
    //when no routes were matched...
    this.app.use("*", not_found);

    // central error handling
    this.app.use(addErrorLog);
    this.app.use(sendErrorMessage);
  }

  addRoutes() {
    this.app.use("/api/songs", song_router);
    this.app.use("/api/artists", artist_router);
    this.app.use("/api/playlists", playlist_router);


  }

  addMiddlewares() {
    this.app.use(cors());
    this.app.use(morgan("dev"));
    this.app.use((req, res, next) => {
      logger.addLog(`${req.method} ${req.path} ${Date.now()}\n`);
      next();
    });
  }

  async startServer() {
    //connect to mongo db
    await connect_db(String(DB_URI));
    this.app.listen(Number(PORT), HOST);
    log.magenta("api is live on", ` ✨ ⚡  http://${HOST}:${PORT} ✨ ⚡`);
  }
}

const serverInstance = new apiUserServer();
serverInstance.startServer().catch(console.log);
