import { IArtist, ArtistDetailsForUpdate } from "../../utilities/types.js";
import * as artist_model from "./artist.db-handler.js";


export async function createArtist(artistData: IArtist) {
    const artist = await artist_model.createArtist(artistData);
    return artist;
}

export async function getAllArtists() {
    const artists = await artist_model.getAllArtists();
    return artists;
}

export async function getArtistsPagination(pagination: number, limit: number) {
    const artists = await artist_model.getArtistsPagination(pagination,limit);
    return artists;
}

export async function getSingleArtist(id: string) {
    const artist = await artist_model.getSingleArtist(id);
    return artist;
};

export async function updateSingleArtist(id: string, artistData: ArtistDetailsForUpdate) {
    const artist = await artist_model.updateSingleArtist(id, artistData);
    return artist;
};

export async function deleteArtist(id: string) {
    const artist = await artist_model.deleteArtist(id);
    return artist;
};