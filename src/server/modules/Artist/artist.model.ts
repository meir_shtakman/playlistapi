import mongoose from "mongoose";
const { Schema, model } = mongoose;

const ArtistSchema = new Schema({
    first_name: {
        type: String, validate: {
            validator: function (v: string) {
                return /[a-zA-Z]{2,30}/.test(v);
            },
            message: (props: any) => `${props.value} is not a valid first name!`
        },
        required: [true, "User first name required"]
    },
    last_name: {
        type: String, validate: {
            validator: function (v: string) {
                return /[a-zA-Z]{2,30}/.test(v);
            },
            message: (props: any) => `${props.value} is not a valid last name!`
        },
        required: [true, "User last name required"]
    },
    songs: [{
        type: Schema.Types.ObjectId, ref:"songs"
    }],
},{timestamps:true});

export default model("artists", ArtistSchema);