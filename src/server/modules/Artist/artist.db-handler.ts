import artist_model from "./artist.model.js";
import { ArtistDetailsForUpdate, IArtist } from "../../utilities/types.js";
import { deleteSong } from "../Song/song.db-handler.js";

export async function checkUnicArtist(body: ArtistDetailsForUpdate, id: string) {
    let first_name;
    let last_name;
    if (id) {
        const artistBeforeChange = artist_model.findById(id);
        if (artistBeforeChange) {
            first_name = body.first_name ? body.first_name : artistBeforeChange.first_name;
            last_name = body.last_name ? body.last_name : artistBeforeChange.last_name;
        }
    } else {
        first_name = body.first_name;
        last_name = body.last_name;
    }
    const artist = await artist_model.findOne({ first_name, last_name }).exec();
    console.log(artist,first_name,last_name);
    return !artist;
}

export async function createArtist(songData: IArtist) {
    const artist = await artist_model.create(songData);
    return artist;
};

export async function getAllArtists() {
    const artist = await artist_model.find()
        // .select(`-__v`);
        .select(`_id 
            first_name 
            last_name 
            songs`).populate("songs");
    return artist;
}

export async function getArtistsPagination(pagination: number, limit: number) {
    const artists = await artist_model.find().skip(pagination > 0 ?
        ((pagination - 1) * limit) : 0)
        .limit(limit)
        .select("-__v").populate("songs");;
    return artists;
}

export async function getSingleArtist(id: string) {
    const artist = await artist_model.findById(id)
        .select(`-_id 
    first_name 
    last_name 
    songs`).populate("songs");;
    return artist;
};

export async function updateSingleArtist(id: string, songData: ArtistDetailsForUpdate) {
    const artist = await artist_model.findByIdAndUpdate(id, songData,
        { new: true, upsert: false });
    return artist;
};

export async function deleteArtist(id: string) {
    const artist = await artist_model.findByIdAndRemove(id);
    const pendingSongs = artist.songs.map(async (sid:string) => {
        await deleteSong(sid);
    });
    await Promise.all(pendingSongs);
    return artist;
};