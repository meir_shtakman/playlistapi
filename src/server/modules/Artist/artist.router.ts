/* 
  if there is an error thrown in the DB, asyncMiddleware
  will pass it to next() and express will handle the error */
import raw from "../../middleware/route.async.wrapper.js";
import express from "express";
import { Request, Response } from "express";
import validate from "./artist.validations.js";
import { checkUnicArtist } from "./artist.validations.js";

import { createArtist, deleteArtist, getAllArtists, getSingleArtist, getArtistsPagination, updateSingleArtist } from "./artist.logic.js";
import { HttpError } from "../../utilities/types.js";

const router = express.Router();

router.use(express.json());

router.post("/", validate, checkUnicArtist, raw(async (req, res) => {
  const artist = await createArtist(req.body);
  res.status(200).json(artist);
}));


// GET ALL USERS
router.get("/", raw(async (req, res) => {
  const artists = await getAllArtists();
  res.status(200).json(artists);
})
);

router.get("/:pagination/:limit", raw(async (req, res) => {
  const limit: number = parseInt(req.params.limit);
  const pagination: number = parseInt(req.params.pagination);

  const artists = getArtistsPagination(pagination, limit);

  res.status(200).json(artists);
})
);


// GETS A SINGLE USER
router.get("/:id", raw(async (req, res) => {
  const artist = await getSingleArtist(req.params.id);
  if (!artist) {
    throw new HttpError("artist not found", 400);
  }
  res.status(200).json(artist);
}));
// UPDATES A SINGLE USER
router.put("/:id", validate, checkUnicArtist, raw(async (req, res) => {
  const artist = updateSingleArtist(req.params.id, req.body);
  res.status(200).json(artist);
})
);


// DELETES A USER
router.delete("/:id", raw(async (req: Request, res: Response) => {
  const artist = await deleteArtist(req.params.id);
  if (!artist) throw new HttpError("No artist found.", 400);
  res.status(200).json(artist);
})
);

export default router;
