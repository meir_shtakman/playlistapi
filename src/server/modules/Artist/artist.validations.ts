import Ajv from "ajv";
import { Request, Response, NextFunction } from "express";
import { HttpError } from "../../utilities/types.js";
import {checkUnicArtist as checkIfUnicArtist} from "./artist.db-handler.js";

const ajv = new Ajv();

export default function validateArtistObject(req: Request, res: Response, next: NextFunction) {
  try {
    const data = req.body;
    let valid = true;
    console.log(req.method);
    switch (req.method) {
      case "POST":
        valid = ajv.validate(schemaForPost, data);
        break;
      case "PUT":
        valid = ajv.validate(schemaForPut, data);
        break;
      default:
        break;
    }

    if (!valid) {
      console.log(ajv.errors);
      next(new HttpError(String(`${ajv.errors ? ajv.errors[0].instancePath : undefined} ${ajv.errors ? ajv.errors[0].message : undefined}`), 400));
    } else {
      next();
    }
  } catch (err) {
    next(new HttpError("validation went wrong", 400));
  }
}

export async function checkUnicArtist(req: Request, res: Response, next: NextFunction){
  try{
    const data = req.body;
    const id = req.params.id;
    const valid = await checkIfUnicArtist(data,id);
    if (!valid) {
      console.log(ajv.errors);
      next(new HttpError("There is already artist with this data", 400));
    } else {
      next();
    }

  } catch (err){
    next(new HttpError("validation went wrong", 400));
  }
}

const schemaForPost = {
  type: "object",
  properties: {
    first_name: { type: "string", pattern: "[a-zA-Z]{2,30}" },
    last_name: { type: "string", pattern: "[a-zA-Z]{2,30}" }
  },
  required: ["first_name", "last_name"],
  additionalProperties: true
};

const schemaForPut = {
  type: "object",
  properties: {
    first_name: { type: "string", pattern: "[a-zA-Z]{2,30}" },
    last_name: { type: "string", pattern: "[a-zA-Z]{2,30}" }
  },
  additionalProperties: true
};

