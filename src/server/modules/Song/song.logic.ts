import { ISong, SongDetailsForUpdate } from "../../utilities/types.js";
import * as songDataHandler from "./song.db-handler.js";

export async function createSong(songData: ISong) {
    const user = await songDataHandler.createSong(songData);
    return user;
}

export async function getAllSongs() {
    const songs = await songDataHandler.getAllSongs();
    return songs;
}

export async function getSongsPagination(pagination: number, limit: number) {
    const songs = await songDataHandler.getSongsPagination(pagination,limit);
    return songs;
}

export async function getSingleSong(id: string) {
    const song = await songDataHandler.getSingleSong(id);
    return song;
};

export async function updateSingleSong(id: string, songData: SongDetailsForUpdate) {
    const song = await songDataHandler.updateSingleSong(id, songData);
    return song;
};

export async function deleteSong(id: string) {
    const song = await songDataHandler.deleteSong(id);
    return song;
};