import mongoose from "mongoose";
const { Schema, model } = mongoose;

const SongSchema = new Schema({
    name: {
        type: String, validate: {
            validator: function (v: string) {
                return /[a-zA-Z]{2,30}/.test(v);
            },
            message: (props: any) => `${props.value} is not a valid song name!`
        },
        required: [true, "User first name required"]
    },
    length: {
        type: String, validate: {
            validator: function (v: string) {
                return /^\d{2}:\d{2}$/.test(v);
            },
            message: (props: any) => `${props.value} is not a valid song lentgh!`
        },
        required: [true, "song lentgh required"]
    },
    artists: [{
        type: Schema.Types.ObjectId, ref: "artists"
    }],
    playlists: [{
        type: Schema.Types.ObjectId, ref: "playlists"
    }],
},{timestamps:true});

export default model("songs", SongSchema);