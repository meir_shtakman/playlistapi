import Ajv from "ajv";
import { Request, Response, NextFunction } from "express";
import { HttpError } from "../../utilities/types.js";
import log from "@ajar/marker";

const ajv = new Ajv();

export default function validateSongObject(req: Request, res: Response, next: NextFunction) {
  try {
    const data = req.body;
    let valid = true;
    console.log(req.method);
    switch (req.method) {
      case "POST":
        valid = ajv.validate(schemaForPost, data);
        break;
      case "PUT":
        valid = ajv.validate(schemaForPut, data);
        break;
      default:
        break;
    }

    if (!valid) {
      log.red(ajv.errors);
      next(new HttpError(String(`${ajv.errors ? ajv.errors[0].instancePath : undefined} ${ajv.errors ? ajv.errors[0].message : undefined}`), 400));
    } else {
      next();
    }
  } catch (err) {
    next(new HttpError("validation went wrong", 400));
  }
};

const schemaForPost = {
  type: "object",
  properties: {
    name: { type: "string", pattern: "[a-zA-Z]{2,30}" },
    length: { type: "string", pattern: "^[0-9][0-9][:][0-9][0-9]$" },
    artists: { type: "array" },
    //, pattern: "^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$"
    playlists: { type: "array" }
  },
  required: ["name", "length"],
  additionalProperties: true
};

const schemaForPut = {
  type: "object",
  properties: {
    name: { type: "string", pattern: "[a-zA-Z]{2,30}" },
    length: { type: "string", pattern: "^[0-9][0-9][:][0-9][0-9]$" },
    artists: { type: "array" },
    //, pattern: "^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$"
    playlists: { type: "array" }
  },
  additionalProperties: true
};

