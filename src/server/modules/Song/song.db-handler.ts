import song_model from "./song.model.js";
import artist_model from "../Artist/artist.model.js";
import playlist_model from "../Playlist/playlist.model.js";
import { ISong, SongDetailsForUpdate } from "../../utilities/types.js";

export async function createSong(songData: ISong) {
    const song = await song_model.create(songData);
    console.log("createSong");

    const pendingArtists = songData.artists.map(async id => {
        console.log("in loop");

        const artist = await artist_model.findById(id);
        console.log("get artist");

        artist.songs.push(song.id);
        await artist.save();
    });
    await Promise.all(pendingArtists);

    return song;
};


export async function getAllSongs() {
    const songs = await song_model.find()
        // .select(`-__v`);
        .select(`_id 
            name 
            lentgh 
            artists 
            playlists`).populate("artists").populate("playlists");
    return songs;
}

export async function getSongsPagination(pagination: number, limit: number) {
    const songs = await song_model.find().skip(pagination > 0 ?
        ((pagination - 1) * limit) : 0)
        .limit(limit)
        .select("-__v").populate("artists").populate("playlists");
    return songs;
}

export async function getSingleSong(id: string) {
    const song = await song_model.findById(id)
        .select(`-_id 
    name 
    lentgh 
    artists 
    playlists`).populate("artists").populate("playlists");
    return song;
};

export async function updateSingleSong(id: string, songData: SongDetailsForUpdate) {
    const song = await song_model.findByIdAndUpdate(id, songData,
        { new: true, upsert: false });
    return song;
};

export async function deleteSong(id: string) {
    let song = await getSingleSong(id);
    const pendingArtists = song.artists.map(async (aid: string) => {
        const artist = await artist_model.findById(aid);
        artist.songs.pull(id);
        artist.save();
    });
    const pendingPlaylist = song.playlists.map(async (pid: string) => {
        const playlist = await playlist_model.findById(pid);
        playlist.songs.pull(id);
        playlist.save();
    });
    song = await song_model.findByIdAndRemove(id);
    await Promise.all(pendingArtists);
    await Promise.all(pendingPlaylist);
    return song;
};