/* 
  if there is an error thrown in the DB, asyncMiddleware
  will pass it to next() and express will handle the error */
import raw from "../../middleware/route.async.wrapper.js";
import express from "express";
import { Request, Response } from "express";
import validate from "./song.validations.js";
import { createSong, getAllSongs, getSongsPagination, getSingleSong, deleteSong, updateSingleSong } from "./song.logic.js";
import { HttpError } from "../../utilities/types.js";

const router = express.Router();

router.use(express.json());

router.post("/", validate, raw(async (req, res) => {
  console.log("before creation");

  const song = await createSong(req.body);
  console.log("after creation");
  res.status(200).json(song);
}));


router.get("/", raw(async (req, res) => {
  const songs = await getAllSongs();
  res.status(200).json(songs);
})
);

router.get("/:pagination/:limit", raw(async (req, res) => {
  const limit: number = parseInt(req.params.limit);
  const pagination: number = parseInt(req.params.pagination);

  const songs = getSongsPagination(pagination, limit);

  res.status(200).json(songs);
})
);


// GETS A SINGLE USER
router.get("/:id", raw(async (req, res) => {
  const song = await getSingleSong(req.params.id);
  if (!song) {
    throw new HttpError("Song not found", 400);
  }
  res.status(200).json(song);
}));
// UPDATES A SINGLE USER
router.put("/:id", validate, raw(async (req, res) => {
  const song = await updateSingleSong(req.params.id, req.body);
  res.status(200).json(song);
})
);


// DELETES A USER
router.delete("/:id", raw(async (req: Request, res: Response) => {
  const song = await deleteSong(req.params.id);
  if (!song) throw new HttpError("Song not found.", 400);
  res.status(200).json(song);
})
);

export default router;
