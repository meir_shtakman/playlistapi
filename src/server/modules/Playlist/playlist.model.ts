import mongoose from "mongoose";
const { Schema, model } = mongoose;

const PlayListSchema = new Schema({
    name: {
        type: String, validate: {
            validator: function (v: string) {
                return /[a-zA-Z]{2,30}/.test(v);
            },
            message: (props: any) => `${props.value} is not a valid playlist name!`
        },
        required: [true, "playlist name required"]
    },
    songs: [{
        type: Schema.Types.ObjectId, ref:"songs"
    }],
},{timestamps:true});

export default model("playlists", PlayListSchema);