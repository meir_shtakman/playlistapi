import playlist_model from "./playlist.model.js";
import song_model from "../Song/song.model.js";
import { IPlaylist, PlaylistDetailsForUpdate } from "../../utilities/types.js";

export async function checkUnicPlaylist(body: PlaylistDetailsForUpdate, id: string) {
    let name;
    if (id) {
        const playlistBeforeChange = playlist_model.findById(id);
        if (playlistBeforeChange) {
            name = body.name ? body.name : playlistBeforeChange.name;
        }
    } else {
        name = body.name;
    }
    const playlist = await playlist_model.findOne({ name }).exec();
    return !playlist;
}


export async function createPlaylist(playlistData: IPlaylist){
    const playlist = await playlist_model.create(playlistData);
    return playlist;
};

export async function addSongToPlaylist(playlistId:string,songId:string){
    const playlist = await playlist_model.findById(playlistId);
    playlist.songs.push(songId);
    await playlist.save();
    const song = await song_model.findById(songId);
    await song.playlists.push(playlistId);
    await song.save();
    return playlist;
}

export async function getAllPlaylists() {
    const playlist = await playlist_model.find()
        // .select(`-__v`);
        .select(`_id 
            name 
            songs`).populate("songs");
    return playlist;
}

export async function getPlaylistsPagination(pagination: number, limit: number) {
    const playlist = await playlist_model.find().skip(pagination > 0 ?
        ((pagination - 1) * limit) : 0)
        .limit(limit)
        .select("-__v").populate("songs");
    return playlist;
}

export async function getSinglePlaylist(id: string) {
    const playlist = await playlist_model.findById(id)
    .select(`-_id 
    name 
    songs`).populate("songs");
    return playlist;
};

export async function updateSinglePlaylist(id: string, playlistData: PlaylistDetailsForUpdate) {
    const playlist = await playlist_model.findByIdAndUpdate(id,playlistData ,
        { new: true, upsert: false });
    return playlist;
};

export async function deletePlaylist(id: string) {
    const playlist = await playlist_model.findByIdAndRemove(id);
    const pendingSongs = playlist.songs.map(async (sid:string) => {
        const song = await song_model.findById(sid);
        song.playlists.pull(id);
        song.save();
    });
    await Promise.all(pendingSongs);

    return playlist;
}; 