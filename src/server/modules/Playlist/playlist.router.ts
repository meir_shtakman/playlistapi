/* 
  if there is an error thrown in the DB, asyncMiddleware
  will pass it to next() and express will handle the error */
import raw from "../../middleware/route.async.wrapper.js";
import express from "express";
import { Request, Response } from "express";
import validate from "./playlist.validations.js";
import {checkUnicPlaylist} from "./playlist.validations.js";

import {addSongToPlaylist, createPlaylist, deletePlaylist, getAllPlaylists, getSinglePlaylist, getPlaylistsPagination, updateSinglePlaylist } from "./playlist.logic.js";
import { HttpError } from "../../utilities/types.js";

const router = express.Router();

// parse json req.body on post routes
router.use(express.json());

// CREATES A NEW USER

router.post("/:pid/song/:sid", raw(async (req, res) => {
  const playlist = await addSongToPlaylist(req.params.pid,req.params.sid);
  res.status(200).json(playlist);
}));

router.post("/", validate,checkUnicPlaylist, raw(async (req, res) => {
  const playlist = await createPlaylist(req.body);
  res.status(200).json(playlist);
}));



router.get("/", raw(async (req, res) => {
  const playlists = await getAllPlaylists();
  res.status(200).json(playlists);
})
);

router.get("/:pagination/:limit", raw(async (req, res) => {
  const limit: number = parseInt(req.params.limit);
  const pagination: number = parseInt(req.params.pagination);

  const playlist = getPlaylistsPagination(pagination, limit);

  res.status(200).json(playlist);
})
);


// GETS A SINGLE USER
router.get("/:id", raw(async (req, res) => {
  const playlist = await getSinglePlaylist(req.params.id);
  if (!playlist) {
    throw new HttpError("playlist not found", 400);
  }
  res.status(200).json(playlist);
}));
// UPDATES A SINGLE USER
router.put("/:id", validate,checkUnicPlaylist, raw(async (req, res) => {
  const playlist = updateSinglePlaylist(req.params.id, req.body);
  res.status(200).json(playlist);
})
);


// DELETES A USER
router.delete("/:id", raw(async (req: Request, res: Response) => {
  const playlist = await deletePlaylist(req.params.id);
  if (!playlist) throw new HttpError("No playlist found.", 400);
  res.status(200).json(playlist);
})
);

export default router;
