import { IPlaylist, PlaylistDetailsForUpdate } from "../../utilities/types.js";
import * as playlistDataHandler from "./playlist.db-handler.js";


export async function createPlaylist(playlistData: IPlaylist) {
    const playlist = await playlistDataHandler.createPlaylist(playlistData);
    return playlist;
}

export async function addSongToPlaylist(playlistId:string,songId:string){
    const playlist = await playlistDataHandler.addSongToPlaylist(playlistId,songId);
    return playlist;
}

export async function getAllPlaylists() {
    const playlists = await playlistDataHandler.getAllPlaylists();
    return playlists;
}

export async function getPlaylistsPagination(pagination: number, limit: number) {
    const playlists = await playlistDataHandler.getPlaylistsPagination(pagination,limit);
    return playlists;
}

export async function getSinglePlaylist(id: string) {
    const playlist = await playlistDataHandler.getSinglePlaylist(id);
    return playlist;
};

export async function updateSinglePlaylist(id: string, playlistData: PlaylistDetailsForUpdate) {
    const playlist = await playlistDataHandler.updateSinglePlaylist(id, playlistData);
    return playlist;
};

export async function deletePlaylist(id: string) {
    const playlist = await playlistDataHandler.deletePlaylist(id);
    return playlist;
};