import Ajv from "ajv";
import { Request, Response, NextFunction } from "express";
import { HttpError } from "../../utilities/types.js";
import log from "@ajar/marker";
import { checkUnicPlaylist as checkIfUnicPlaylist } from "./playlist.db-handler.js";

const ajv = new Ajv();

export default function validatePlaylistObject(req: Request, res: Response, next: NextFunction) {
  try {
    const data = req.body;
    let valid = true;
    switch (req.method) {
      case "POST":
        valid = ajv.validate(schemaForPost, data);
        break;
      case "PUT":
        console.log("before Put");
        valid = ajv.validate(schemaForPut, data);
        console.log("After Put");
        break;
      default:
        break;
    }

    if (!valid) {
      next(new HttpError(String(`${ajv.errors ? ajv.errors[0].instancePath : undefined} ${ajv.errors ? ajv.errors[0].message : undefined}`), 400));
    } else {
      next();
    }
  } catch (err) {
    log.red(err);
    next(new HttpError("validation went wrong", 400));
  }
}
export async function checkUnicPlaylist(req: Request, res: Response, next: NextFunction){
  try{
    const data = req.body;
    const id = req.params.id;
    const valid = await checkIfUnicPlaylist(data,id);
    if (!valid) {
      console.log(ajv.errors);
      next(new HttpError("There is already playlist with this data", 400));
    } else {
      next();
    }

  } catch (err){
    next(new HttpError("validation went wrong", 400));
  }
}
const schemaForPost = {
  type: "object",
  properties: {
    name: { type: "string", pattern: "[a-zA-Z]{2,30}" },
    songs: { type: "array" }
  },
  required: ["name"],
  additionalProperties: false
};

const schemaForPut = {
  type: "object",
  properties: {
    name: { type: "string", pattern: "[a-zA-Z]{2,30}" },
    songs: { type: "array" }
  },
  additionalProperties: true
};

